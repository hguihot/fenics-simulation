# fenics-simulation

Fenics simulation for bristle displacement or others. 

To install Fenics go to : 
https://fenicsproject.org/download/

(I think the best solution right now is the Docker install)

Then to actually view the results, use paraview : https://www.paraview.org/

